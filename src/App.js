import About from "./components/About";
import Banner from "./components/Banner";
import Footer from "./components/Footer";
import Links from "./components/Links";
import Roadmap from "./components/Roadmap";
import 'aos/dist/aos.css'
import Teams from "./components/Teams";

function App() {
  return (
    <>
      <Banner />
      <Links />
      <About />
      <Roadmap />
      <Teams />
      <Footer />
    </>
  );
}

export default App;
