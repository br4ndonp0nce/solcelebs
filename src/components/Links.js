import React, { useState } from "react";
//
const Links = () => {
  const [ani1, setAni1] = useState(false);
  const [ani2, setAni2] = useState(false);
  const [ani3, setAni3] = useState(false);

  return (
    <div className="with-gif pt-5">
      <div className="container">
        <h3 className="text-white text-center mt-5">
          Join The SolCelebs Community
        </h3>
        <ul className="social-icon">
          <li className="social-icon__item">
            <a
              className="social-icon__link"
              href="https://t.me/BNBPunks"
              target="_blank"
            >
              <i className="fab fa-telegram"></i>
            </a>
          </li>
          <li className="social-icon__item">
            <a
              className="social-icon__link"
              href="https://discord.com/invite/SolCelebs"
              target="_blank"
            >
              <i className="fab fa-discord"></i>
            </a>
          </li>
          <li className="social-icon__item">
            <a
              className="social-icon__link"
              href="https://twitter.com/SolCelebs"
              target="_blank"
            >
              <i className="fab fa-twitter"></i>
            </a>
          </li>
          <li className="social-icon__item">
            <a
              className="social-icon__link"
              href="https://www.instagram.com/solcelebs/"
              target="_blank"
            >
              <i className="fab fa-instagram"></i>
            </a>
          </li>
        </ul>
        <div className="row justify-content-center text-center pt-5">
          <div className="col-md-6">
            <img
              src="/assets/img/solcelebs.gif"
              alt=""
              style={{ borderRadius: "50px", width: "90%" }}
            />
          </div>
          <div className="col-md-6 text-center">
            <h3 className="card-roadmap-details">View Collections On:</h3>
            <div className="mt-4">
              <button
                className="btn btn-lg btn-special"
                onMouseEnter={() => {
                  setAni1(true);
                }}
                onMouseLeave={() => {
                  setAni1(false);
                }}
              >
                Solanart (Applied){" "}
                <i
                  className={
                    ani1
                      ? "fa fa-arrow-circle-right ml-3"
                      : "fa fa-arrow-circle-right"
                  }
                ></i>
              </button>
            </div>
            <div className="mt-4">
              <button
                className="btn btn-lg btn-special"
                onMouseEnter={() => {
                  setAni2(true);
                }}
                onMouseLeave={() => {
                  setAni2(false);
                }}
              >
                MagicEden (Approved){" "}
                <i
                  className={
                    ani2
                      ? "fa fa-arrow-circle-right ml-3"
                      : "fa fa-arrow-circle-right"
                  }
                ></i>
              </button>
            </div>
            <div className="mt-4">
              <button
                className="btn btn-lg btn-special"
                onMouseEnter={() => {
                  setAni3(true);
                }}
                onMouseLeave={() => {
                  setAni3(false);
                }}
              >
                AlphaArt (Approved){" "}
                <i
                  className={
                    ani3
                      ? "fa fa-arrow-circle-right ml-3"
                      : "fa fa-arrow-circle-right"
                  }
                ></i>
              </button>
            </div>
            <div className="col-12" style={{ marginTop: "50px" }}>
              <a
                href="https://sol-celebs-mint.vercel.app/Mint"
                target="_blank"
                className="text-center btn btn-very-special"
              >
                Launching 10/30
              </a>
              <p className="text-white mt-3 mb-5">
                Join the whitelist to secure your mint by joining our discord!
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="row" style={{ marginTop: "8rem" }}>
        <div className="col-12">
          <div className="custom-shape-divider-bottom-1634045101">
            <svg
              data-name="Layer 1"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 1200 120"
              preserveAspectRatio="none"
            >
              <path
                d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z"
                className="shape-fill"
              ></path>
            </svg>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Links;
