import React, { useEffect } from 'react'
import { animated, useSpring } from "react-spring";
import Aos from "aos";
import 'aos/dist/aos.css'


const About = () => {
    const spring = useSpring({ config: { duration: 5000 }, from: { val: 0 }, to: { val: 4597 } });
    useEffect(() => {
        Aos.init({ duration: 1500 })
    })
    return (
        <div className="about pt-5">
            <div data-aos="fade-up" className="container">
                <h1 className="text-center">Quick Summary of SolCelebs</h1>
                <div className="row justify-content-center text-center pt-5">
                    <div className="col-md-4 my-5">
                        <div>
                            <h2>
                                <animated.div>
                                    {spring.val.to(val => Math.floor(val))}
                                </animated.div>
                            </h2>
                            <h5>SolCelebs Minted</h5>
                        </div>
                    </div>
                    <div className="col-md-8 text-center">
                        <div className="card card-about">
                            <div className="card-body">
                                <p>SolCelebs  is a collection of 10,000 randomly generated NFT’s. Everyone has a chance to get their own SolCelebs Alien, Zombie, or Ape but you won’t know what you get until you mint. All of our SolCelebs  are generated using a defined rarity system and AI art generation. Buying a Sol Celeb allows you special access to all of our future projects we are working on with celebrities that include Juicy J, Soulja Boy, and more! Once the 10,000 are gone you will never be able to get your hands on one again at mint price. Don’t miss out on something that could be your next watch, car, or house.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row pt-5" style={{ marginTop: '100px' }}>
                <div className="col-12">
                    <div className="custom-shape-divider-bottom-1634048622">
                        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                            <path d="M1200 0L0 0 892.25 114.72 1200 0z" className="shape-fill"></path>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About
