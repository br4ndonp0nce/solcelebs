import React from 'react'

const Footer = () => {
    return (
        <footer className="footer">
            <div className="waves">
                <div className="wave" id="wave1"></div>
                <div className="wave" id="wave2"></div>
                <div className="wave" id="wave3"></div>
                <div className="wave" id="wave4"></div>
            </div >
            <h3 className="text-white text-center">Join The SolCelebs Community</h3>
            <ul className="social-icon">
                <li className="social-icon__item">
                    <a className="social-icon__link" href="https://t.me/BNBPunks" target="_blank">
                        <i className="fab fa-telegram"></i>
                    </a>
                </li>
                <li className="social-icon__item">
                    <a className="social-icon__link" href="https://discord.com/invite/SolCelebs" target="_blank">
                        <i className="fab fa-discord"></i>
                    </a>
                </li>
                <li className="social-icon__item">
                    <a className="social-icon__link" href="https://twitter.com/SolCelebs" target="_blank">
                        <i className="fab fa-twitter"></i>
                    </a>
                </li>
                <li className="social-icon__item">
                    <a className="social-icon__link" href="https://www.instagram.com/solcelebs/" target="_blank">
                        <i className="fab fa-instagram"></i>
                    </a>
                </li>
            </ul>
            <a href="#"><img src="/assets/img/logo.png" alt="logo" width="300" /></a>
            <p>&copy; {new Date().getFullYear()} <a href="#">SolCelebs</a> | All Rights Reserved</p>
        </footer >
    )
}

export default Footer
