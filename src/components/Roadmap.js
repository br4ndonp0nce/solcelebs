import React, { useEffect } from 'react'
import Aos from "aos";
import 'aos/dist/aos.css'

const Roadmap = () => {
    useEffect(() => {
        Aos.init({ duration: 2000 })
    })
    return (
        <div className="roadmap py-5">
            <div data-aos="fade-up" className="container">
                <h1 className="text-center">Roadmap</h1>
                <p className="text-center">Our SolCelebs team has put together a marketing roadmap to help the project grow to the massive scale we dream of. We want to have a marketing plan like no other NFT project has ever done before working with massive influencers and expansive digital ad campaigns.</p>
                <div data-aos="fade-up" className="row pt-5">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header card-header-yellow">
                                <div className="cir cir-red"></div>
                                <div className="cir cir-green"></div>
                                <div className="cir cir-grey"></div>
                            </div>
                            <div className="card-body">
                                <div className="row py-5">
                                    <div className="col-md-4">
                                        <div className="card-number" style={{ padding: '30px 0 20px 0', background: '#fcfa7b', borderRadius: '20px' }}>
                                            <h1 className="text-center">10%</h1>
                                        </div>
                                    </div>
                                    <div className="col-md-8 card-roadmap-details">
                                        <ul className="pl-3">
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Juicy J/Soulja Boy/Kyle Massey/Shiggy show Promos</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Closing a deal with high-profile rapper who will personally be given 5 SolCelebs</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Minor Twitter crypto influencers, such as london crypto, crypto monkey, etc</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Telegram paid shilling, marketing</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Telegram Paid Promo Channels</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Yahoo News and Marketwatch Articles</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Constant Discord and Telegram giveaways!</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" className="row pt-5">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header card-header-purple">
                                <div className="cir cir-red"></div>
                                <div className="cir cir-green"></div>
                                <div className="cir cir-grey"></div>
                            </div>
                            <div className="card-body">
                                <div className="row py-5">
                                    <div className="col-md-4">
                                        <div className="card-number" style={{ padding: '30px 0 20px 0', background: '#bd10e0', borderRadius: '20px' }}>
                                            <h1 className="text-center text-white">25%</h1>
                                        </div>
                                    </div>
                                    <div className="col-md-8 card-roadmap-details">
                                        <ul className="pl-3">
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Giveaway! 5 sol + 10 nfts</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> More news articles, on various different sites</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Paid Twitter Promos</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Constant AMAs</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Expansive paid advertisement campaign on Google and YouTube ads.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" className="row pt-5">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header card-header-blue">
                                <div className="cir cir-red"></div>
                                <div className="cir cir-green"></div>
                                <div className="cir cir-grey"></div>
                            </div>
                            <div className="card-body">
                                <div className="row py-5">
                                    <div className="col-md-4">
                                        <div className="card-number" style={{ padding: '30px 0 20px 0', background: '#007adf', borderRadius: '20px' }}>
                                            <h1 className="text-center text-white">50%</h1>
                                        </div>
                                    </div>
                                    <div className="col-md-8 card-roadmap-details">
                                        <ul className="pl-3">
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Giveaway - 7 sol + 7  SolCelebs nfts</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Chinese Marketing, BTOK, Weibo, ETC</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> More influencers</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Possible Juicy J or Soulja Boy AMA</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Heavy billboard marketing in Los Angeles, CA</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Heavy Twitter Campaigns</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" className="row pt-5">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header card-header-red">
                                <div className="cir cir-red"></div>
                                <div className="cir cir-green"></div>
                                <div className="cir cir-grey"></div>
                            </div>
                            <div className="card-body">
                                <div className="row py-5">
                                    <div className="col-md-4">
                                        <div className="card-number" style={{ padding: '30px 0 20px 0', background: '#ff2828', borderRadius: '20px' }}>
                                            <h1 className="text-center text-white">75%</h1>
                                        </div>
                                    </div>
                                    <div className="col-md-8 card-roadmap-details">
                                        <ul className="pl-3">
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Giveaway! - 10 sol + 7 SolCelebs nft</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Desiigner Partnership</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Soulja Boy Instagram & Twitter partnership</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> TheShiggyShow (Instagram) partnership</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Gillie Da King partnership</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Boosie Partnership</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> And many more Partnerships</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" className="row pt-5">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header card-header-green">
                                <div className="cir cir-red"></div>
                                <div className="cir cir-green"></div>
                                <div className="cir cir-grey"></div>
                            </div>
                            <div className="card-body">
                                <div className="row py-5">
                                    <div className="col-md-4">
                                        <div className="card-number" style={{ padding: '30px 0 20px 0', background: '#00964b', borderRadius: '20px' }}>
                                            <h1 className="text-center text-white">100%</h1>
                                        </div>
                                    </div>
                                    <div className="col-md-8 card-roadmap-details">
                                        <ul className="pl-3">
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Giveaway! - 15 sol + 10 SolCelebs nfts</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Massive Instagram campaign with over 10 influential influencers that amass a total of 20-50 million followers</li>
                                            <li data-aos="fade-right" data-aos-duration="3000"><i className="fa fa-angle-right"></i> Roadmap 2.0 Begins</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Roadmap
